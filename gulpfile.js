const gulp = require('gulp');


const changed = require('gulp-changed');


const DEST = './app';


gulp.task('build', function () {
    return gulp.src(['src/**/*', 'node_modules/**/*',  'package.json'], {
            base: '.'
        })
        .pipe(changed(DEST))
        .pipe(gulp.dest(DEST));
});







