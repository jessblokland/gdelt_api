const config = require('./config.js');
const logger = config.logger;
const _ = require('lodash');


const Server = function () {
    this.app = null;
};
let server = new Server();
module.exports = server;


const requestProcessor = require('./requestprocessor.js');
const express = require('express');
var bodyParser = require('body-parser');
const app = express();
server.app = app;
const jsonParser = bodyParser.json();
const validator = require('./requestvalidator.js');
const {
    check,
    validationResult
} = require('express-validator/check');



// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({
    extended: false
})




Server.prototype.startServer = function () {
    //handle needed for integration testing
    this.serverHandle = this.app.listen(config.port, function () {
    });

    logger.info('server listening on port ' + config.port);

};

Server.prototype.addRoutes = function () {

    logger.debug('adding routes');
    var that = this;

    this.app.get('/', async function (req, res, next) {
        try {
            // const errors = validationResult(req);
            // if (!errors.isEmpty()) {
            //     return res.status(400).json({
            //         errors: errors.mapped()
            //     });
            // }
            logger.debug('GET / : ');
            res.status(200).send('Processing GDELT Fulltext query');
             



        } catch (e) {
            logger.error(e.message);
            res.status(500).send('Internal Server Error:' + e.message);
        }

    });

};