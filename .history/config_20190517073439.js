const winston = require("winston");
require("winston-daily-rotate-file");

let config = {
	logger: winston.createLogger()
};

config.logger.exitOnError = false;


config.gdeltUri = "";
config.port = 2424;

module.exports = config;