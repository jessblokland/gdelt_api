const config = require('./config.js');
const logger = config.logger;
const request = require('request-promise-native');
// const _ = require('lodash');


const Server = function () {
    this.app = null;
};
let server = new Server();
module.exports = server;


const requestProcessor = require('./requestprocessor.js');
const express = require('express');
var bodyParser = require('body-parser');
const app = express();
server.app = app;
const jsonParser = bodyParser.json();





// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({
    extended: false
})




Server.prototype.startServer = function () {
    //handle needed for integration testing
    this.serverHandle = this.app.listen(config.port, function () {
    });

    logger.info('server listening on port ' + config.port);

};

Server.prototype.addRoutes = function () {

    logger.debug('adding routes');
    var that = this;

    this.app.get('/', async function (req, res, next) {
        try {
            // const errors = validationResult(req);
            // if (!errors.isEmpty()) {
            //     return res.status(400).json({
            //         errors: errors.mapped()
            //     });
            // }
            logger.debug('GET / : ');
            logger.debug(JSON.stringify(req.headers));
            
            let options = {
                uri: "http://api.gdeltproject.org/api/v2/doc/doc?query=domain:foxnews.com&mode=ArtList&&maxrecords=200&timespan=1day&format=json",
                method: 'GET',
                json: true,
                headers: {}
                
            }
            options.headers['x-request-id'] = req.headers['x-request-id'];
            options.headers['x-b3-traceid'] = req.headers['x-b3-trace'];
            options.headers['x-b3-spanid'] = req.headers['x-b3-spanid'];
            options.headers['x-b3-parentspanid'] = req.headers['x-b3-parentspanid'];
            options.headers['x-b3-sampled'] = req.headers['x-b3-sampled'];
            options.headers['x-b3-flags'] = req.headers['x-b3-flags'];
            options.headers['x-ot-span-context'] = req.headers['x-ot-span-context'];





            logger.debug(JSON.stringify(options));
            let response = await request(options) 
            res.status(200).send(response);
             



        } catch (e) {
            logger.error(e.message);
            res.status(500).send('Internal Server Error:' + e.message);
        }

    });

};