const winston = require("winston");
require("winston-daily-rotate-file");

let config = {
	logger: winston.createLogger()
};

//TODO how to handle logs in AWS? start by logging to console
config.logger.add(new winston.transports.Console({
    level: "debug",
    handleExceptions: true,
    humanReadableUnhandledException: true,
    timestamp: true
}));

config.logger.exitOnError = false;


config.gdeltUri = "";
config.port = 5000;

module.exports = config;